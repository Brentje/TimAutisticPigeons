﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitch : StateMachineBehaviour
{

    [SerializeField] protected int sceneToLoad;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // Loads the game scene
        SceneManager.LoadScene(sceneToLoad);
    }
}