﻿using UnityEngine;
using UnityEngine.UI;

public class DeathScreen : UIElement
{
    public int score = 100;
    public Text text;

    public SaveScorePanel SaveScorePanel;

    public override void Enter(UIStack stack)
    {
        base.Enter(stack);
        SeeScore(score);
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            UIStack.PopAll();
            UIStack.Push(UIStack.StartMenu);
        }
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            enabled = false;
            UIStack.Push(SaveScorePanel);
        }
    }

    public override void Exit(UIStack stack)
    {
        base.Exit(stack);
        Minigame.Score = 0;
    }

    public void SeeScore(int score)
    {
        text.text = "*   Your Score was: " + Minigame.Score + ".";
    }
}