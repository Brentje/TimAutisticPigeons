﻿using UnityEngine;

public class MiniGameFinished : UIElement {
    public AudioClip SuccessSound;

    private void Update()
    {
        if (Input.anyKeyDown)
        {
            AudioManager.Play(SuccessSound, AudioType.SFX);

            UIStack.PopAll();
            UIStack.Push(UIStack.StartMenu);
        }
    }
}
