﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shutdown : UIElement
{
    public UIElement BlueScreen;

    public override void Enter(UIStack stack)
    {
        base.Enter(stack);
    }

    public override void Exit(UIStack stack)
    {
        base.Exit(stack);
    }

    public void OpenBlueScreen()
    {
        

        if (BlueScreen == null)
            return;

        UIStack.Push(BlueScreen);
    }
}
