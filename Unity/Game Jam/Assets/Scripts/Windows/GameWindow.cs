﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameWindow : UIElement {
    public List<UIElement> elements;
    public Animator animator;

    public override void Enter(UIStack stack)
    {
        base.Enter(stack);

        animator.SetTrigger("Open");
        stack.Push(elements[0]);
    }

    public override void Exit(UIStack stack)
    {
        base.Exit(stack);
        animator.SetTrigger("Close");
    }
}
