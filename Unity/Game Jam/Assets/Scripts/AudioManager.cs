﻿using UnityEngine;

public class AudioManager : UIElement
{
    public AudioClip[] musicClips;
    public AudioClip[] sfxClips;

    public AudioSource sfxSource, musicSource;
    private static AudioManager manager;

    private void Awake()
    {
        manager = this;
    }

    public static void Play(AudioClip clip, AudioType type)
    {
        if (type == AudioType.SFX)
        {
            manager.sfxSource.PlayOneShot(clip);
        }
        else
        {
            manager.musicSource.Stop();
            manager.musicSource.clip = clip;
            manager.musicSource.Play();
        }
    }

    public static void PlaySFX(int index)
    {
        if (manager)
        {
            if (manager.sfxClips?.Length > index && index > 0)
            {
                Play(manager.sfxClips[index], AudioType.SFX);
            }
        }
    }

    public static void PlayMusic(int index)
    {
        if (manager)
        {
            if (manager.musicClips?.Length > index && index > 0)
            {
                Play(manager.musicClips[index], AudioType.MUSIC);
            }
        }
    }

    public static void StopMusic()
    {
        if (manager)
        {
            manager.musicSource.Stop();
        }
    }
}

public enum AudioType
{
    SFX,
    MUSIC,
}