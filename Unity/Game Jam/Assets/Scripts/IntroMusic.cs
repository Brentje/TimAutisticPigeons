﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroMusic : MonoBehaviour {
    public AudioClip Sound;

    public void PlaySound() {
        AudioManager.Play(Sound, AudioType.SFX);
    }
}
