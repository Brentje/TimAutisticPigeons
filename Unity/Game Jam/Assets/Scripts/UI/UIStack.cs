﻿using System.Collections.Generic;
using Arcade;
using UnityEngine;

public struct UIElementData
{
    public UIElement Menu;
    public UIElement Class;
}

public class UIStack : MonoBehaviour
{
    public UIElement StartMenu;
    public UIElement BlueScreen;
    public UIElement MiniGameFinished;

    private List<UIElementData> stack;

    private void Awake()
    {
        HighScore.Init();
        stack = new List<UIElementData>();
    }

    private void Start()
    {
        if (StartMenu)
            Push(StartMenu);
    }

    public UIElement Push(UIElement element)
    {
        UIElementData data = new UIElementData
        {
            Menu = Instantiate(element),
            Class = element
        };

        stack.Add(data);
        data.Menu.transform.SetParent(transform, false);
        data.Menu.Enter(this);
        return data.Menu;
    }

    public void Pop()
    {
        if (RemoveTop())
            return;

        stack.RemoveAt(stack.Count - 1);

        if (stack.Count == 0)
            return;

        UIElementData top = Peek();
        stack.RemoveAt(stack.Count - 1);

        Push(top.Class);
    }

    public void PopAll()
    {
        if (stack.Count == 0)
            return;

        while (stack.Count > 0)
            RemoveMenu(stack[stack.Count - 1]);
    }

    public UIElementData Peek()
    {
        return stack[stack.Count - 1];
    }

    private void RemoveMenu(UIElementData elementData)
    {
        elementData.Menu.Exit(this);
        Destroy(elementData.Menu.gameObject);
        stack.Remove(elementData);
    }

    public void RemoveMenu(UIElement element)
    {
        for (int i = 0; i < stack.Count; i++)
        {
            if (stack[i].Menu == element)
            {
                RemoveMenu(stack[i]);
                return;
            }
        }
    }

    public void PopFrom(UIElement element)
    {
        for (int i = 0; i < stack.Count; i++)
        {
            if (stack[i].Menu == element)
            {
                while (stack.Count > i)
                    RemoveMenu(stack[stack.Count - 1]);
                return;
            }
        }
    }

    private bool RemoveTop()
    {
        if (stack.Count == 0)
            return true;

        UIElementData data = Peek();
        RemoveMenu(data);

        return false;
    }
}
