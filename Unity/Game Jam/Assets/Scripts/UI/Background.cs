﻿using UnityEngine;
using UnityEngine.UI;

public class Background : UIElement
{
    public Sprite[] BackGrounds;
    public override void Enter(UIStack stack)
    {
        base.Enter(stack);
        GetComponent<Image>().sprite = BackGrounds[Random.Range(0, BackGrounds.Length)];
    }

    public override void Exit(UIStack stack)
    {
        base.Exit(stack);

    }
}
