﻿using UnityEngine;

public class UIElement : MonoBehaviour {
    public UIStack UIStack { get; private set; }

    public virtual void Enter(UIStack stack) {
        UIStack = stack;
    }

    public virtual void Exit(UIStack stack) {

    }

    public virtual void Close()
    {
        UIStack.RemoveMenu(this);
    }
}
