﻿using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

public class MenuButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private Text myText;

    private Button myButton;
    private Image buttonImage;
    private Color myColor;

    public void Awake()
    {
        myButton = gameObject.GetComponent<Button>();
        buttonImage = gameObject.GetComponent<Image>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        myText.color = Color.white;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        myText.color = Color.black;
    }
}
