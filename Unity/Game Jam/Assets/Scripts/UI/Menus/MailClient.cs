﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MailClient : UIElement
{
    public Minigame[] MiniGames;
    public string[] Names;
    public string[] Subjects;

    public AudioClip[] GlitchEffects;

    private string Mail = "From: Brent de Jong\n" +
                          "Sent: Friday, January 26, 2018 8:11 PM\n" +
                          "To: U\n" +
                          "Cc:\n" +
                          "Subject: Will you help us?\n\n" +
                          "Dear friend,\n\n" +
                          "Please pass this to everyone in your address book.\n\n" +
                          "We have a Deli manager from Philadelphia, Pa who has a 13 year old daughter who has been missing for 2 weeks.\nKeep the picture moving on.\nWith luck on her side she will be found.\n\n\"I am asking you all, begging you to please forward this email....\"\n\n"
        ;

    public RectTransform ContactContent;
    public GameObject ContactPrefab;

    public Text MailText;

    private void Start()
    {
        PopulateContactList();
        MailText.text = Mail;
    }

    public override void Enter(UIStack stack) {
        base.Enter(stack);

    }

    public override void Exit(UIStack stack) {
        base.Exit(stack);

    }

    public void PopulateContactList()
    {
        int size = Mathf.Min(Mathf.Min(Names.Length, Subjects.Length), 10);
        List<string> names = new List<string>(Names);
        List<string> subjects = new List<string>(Subjects);
        
        for (int i = 0; i < size; i++)
        {
            Transform contact = Instantiate(ContactPrefab, ContactContent).transform;

            int contactIndex = Random.Range(0, names.Count);
            int subjectIndex = Random.Range(0, subjects.Count);

            contact.GetChild(0).GetComponent<Text>().text = names[contactIndex];
            names.RemoveAt(contactIndex);
            contact.GetChild(1).GetComponent<Text>().text = "Subject: " + subjects[subjectIndex];
            subjects.RemoveAt(subjectIndex);
        }
    }

    public void StartGame()
    {
        AudioManager.Play(GlitchEffects[Random.Range(0, GlitchEffects.Length - 1)], AudioType.SFX);
        UIStack.Push(MiniGames[Random.Range(0, MiniGames.Length)]);
    }
}
