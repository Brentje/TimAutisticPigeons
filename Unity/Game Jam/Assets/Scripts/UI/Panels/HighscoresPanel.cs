﻿using System.Collections;
using System.Collections.Generic;
using Arcade;
using UnityEngine;
using UnityEngine.UI;

public class HighscoresPanel : UIElement
{
    public Text Text;

    public override void Enter(UIStack stack)
    {
        base.Enter(stack);

        string scoreList = "";
        ScoreStruct[] scores = HighScore.GetTop(10);

        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < (i + 1 > 9 ? 0 : 1); j++)
            {
                scoreList += " ";
            }
            scoreList += $"{i + 1}:";
            if (i < scores.Length)
                scoreList += $"   { scores[i].Score} \t-\t{scores[i].Name}";
            scoreList += "\n";
        }
        Text.text = scoreList;
    }
}
