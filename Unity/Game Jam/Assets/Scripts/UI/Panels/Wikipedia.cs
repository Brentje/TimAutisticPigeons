﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.UI;

public class Wikipedia : UIElement {
    public Text Title;
    public Text Text;
    public RawImage Image;

    private Api apiResponse;

    private string wikipediaquery =
            "https://en.wikipedia.org/w/api.php?format=xml&action=query&generator=random&grnlimit=5&prop=extracts|pageimages&pithumbsize=250&exlimit=max&explaintext&exintro"
        ;

    public override void Enter(UIStack stack) {
        base.Enter(stack);
        
        StartCoroutine(GetRandomArticleList());
    }

    public override void Exit(UIStack stack) {
        base.Exit(stack);
    }

    private IEnumerator GetRandomArticleList() {
        using (WWW www = new WWW(wikipediaquery)) {
            yield return www;
            XmlSerializer serializer = new XmlSerializer(typeof(Api));

            using (TextReader reader = new StringReader(www.text)) {
                apiResponse = (Api) serializer.Deserialize(reader);
            }

            BuildWikiPage();
        }
    }

    private void BuildWikiPage() {
        foreach (Page page in apiResponse.Query.Pages.Page) {
            if (page.Title != null && page.Thumbnail != null && page.Extract.Space != null) {
                Title.text = page.Title;
                Text.text = page.Extract.Text;

                StartCoroutine(GetWikiImage(page.Thumbnail.Source));

                return;
            }
            StopAllCoroutines();
            StartCoroutine(GetRandomArticleList());
            return;
        }
    }

    private IEnumerator GetWikiImage(string source) {
        using (WWW www = new WWW(source)) {
            yield return www;
            
            Image.texture = www.texture;
        }
    }
}

[XmlRoot(ElementName = "continue")]
public class Continue {
    [XmlAttribute(AttributeName = "grncontinue")]
    public string Grncontinue { get; set; }
    [XmlAttribute(AttributeName = "continue")]
    public string _continue { get; set; }
}

[XmlRoot(ElementName = "extracts")]
public class Extracts {
    [XmlAttribute(AttributeName = "space", Namespace = "http://www.w3.org/XML/1998/namespace")]
    public string Space { get; set; }
    [XmlText]
    public string Text { get; set; }
}

[XmlRoot(ElementName = "warnings")]
public class Warnings {
    [XmlElement(ElementName = "extracts")]
    public Extracts Extracts { get; set; }
}

[XmlRoot(ElementName = "extract")]
public class Extract {
    [XmlAttribute(AttributeName = "space", Namespace = "http://www.w3.org/XML/1998/namespace")]
    public string Space { get; set; }
}

[XmlRoot(ElementName = "thumbnail")]
public class Thumbnail {
    [XmlAttribute(AttributeName = "source")]
    public string Source { get; set; }
    [XmlAttribute(AttributeName = "width")]
    public string Width { get; set; }
    [XmlAttribute(AttributeName = "height")]
    public string Height { get; set; }
}

[XmlRoot(ElementName = "page")]
public class Page {
    [XmlElement(ElementName = "extract")]
    public Extracts Extract { get; set; }
    [XmlElement(ElementName = "thumbnail")]
    public Thumbnail Thumbnail { get; set; }
    [XmlAttribute(AttributeName = "_idx")]
    public string _idx { get; set; }
    [XmlAttribute(AttributeName = "pageid")]
    public string Pageid { get; set; }
    [XmlAttribute(AttributeName = "ns")]
    public string Ns { get; set; }
    [XmlAttribute(AttributeName = "title")]
    public string Title { get; set; }
    [XmlAttribute(AttributeName = "pageimage")]
    public string Pageimage { get; set; }
}

[XmlRoot(ElementName = "pages")]
public class Pages {
    [XmlElement(ElementName = "page")]
    public List<Page> Page { get; set; }
}

[XmlRoot(ElementName = "query")]
public class Query {
    [XmlElement(ElementName = "pages")]
    public Pages Pages { get; set; }
}

[XmlRoot(ElementName = "limits")]
public class Limits {
    [XmlAttribute(AttributeName = "extracts")]
    public string Extracts { get; set; }
}

[XmlRoot(ElementName = "api")]
public class Api {
    [XmlElement(ElementName = "continue")]
    public Continue Continue { get; set; }
    [XmlElement(ElementName = "warnings")]
    public Warnings Warnings { get; set; }
    [XmlElement(ElementName = "query")]
    public Query Query { get; set; }
    [XmlElement(ElementName = "limits")]
    public Limits Limits { get; set; }
    [XmlAttribute(AttributeName = "batchcomplete")]
    public string Batchcomplete { get; set; }
}