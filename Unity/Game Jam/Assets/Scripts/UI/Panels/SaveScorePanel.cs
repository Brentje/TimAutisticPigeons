﻿using Arcade;
using UnityEngine.UI;

public class SaveScorePanel : UIElement
{
    public InputField Name;
    public Text ErrorText;

    public void Save()
    {
        if (string.IsNullOrWhiteSpace(Name.text))
        {
            ErrorText.text = "Error 406: Name not acceptable";
            return;
        }

        HighScore.AddScore(Minigame.Score, Name.text);
        UIStack.PopAll();
        UIStack.Push(UIStack.StartMenu);
    }
}
