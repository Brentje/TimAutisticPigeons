﻿using System.Net.Mime;
using UnityEngine;

public class StartPanel : UIElement
{
    public UIElement blueScreenPanel;
    public UIElement documentsPanel;

    public UIElement gameWindow;

    private UIElement activeElement;

    public UIElement programsPanel;
    public UIElement settingsPanel;

    public override void Enter(UIStack stack)
    {
        base.Enter(stack);
    }

    public override void Exit(UIStack stack)
    {
        base.Exit(stack);
    }

    public override void Close()
    {
        UIStack.PopFrom(this);
    }

    public void OpenPrograms()
    {
        if (programsPanel == null)
            return;

        if (activeElement)
        {
            UIStack.RemoveMenu(activeElement);
            activeElement = null;
        }

        activeElement = UIStack.Push(programsPanel);
    }

    public void OpenDocuments()
    {
        if (documentsPanel == null)
            return;

        if (activeElement)
        {
            UIStack.RemoveMenu(activeElement);
            activeElement = null;
        }

        activeElement = UIStack.Push(documentsPanel);
    }

    public void OpenSettings()
    {
        if (settingsPanel == null)
            return;

        if (activeElement)
        {
            UIStack.RemoveMenu(activeElement);
            activeElement = null;
        }

        activeElement = UIStack.Push(settingsPanel);
    }

    public void OpenBlueScreen()
    {
        if (blueScreenPanel == null)
            return;

        if (activeElement)
        {
            UIStack.RemoveMenu(activeElement);
            activeElement = null;
        }

        activeElement = UIStack.Push(blueScreenPanel);
    }

    public void Run()
    {
        UIStack.Push(gameWindow);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}