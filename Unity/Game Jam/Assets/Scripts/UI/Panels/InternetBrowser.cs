﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InternetBrowser : UIElement
{
    public UIElement wikipedia;

    public override void Enter(UIStack stack)
    {
        base.Enter(stack);
    }

    public override void Exit(UIStack stack)
    {
        base.Exit(stack);
    }

    public void OpenWiki()
    {
        if (wikipedia == null)
            return;

        UIStack.Push(wikipedia);
    }
}
