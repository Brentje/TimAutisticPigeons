﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsSubPanel : UIElement
{
    public UIElement audioPanel;

    public override void Enter(UIStack stack)
    {
        base.Enter(stack);
    }

    public override void Exit(UIStack stack)
    {
        base.Exit(stack);
    }

    public void OpenAudioPanel()
    {
        if (audioPanel == null)
            return;

        UIStack.Push(audioPanel);
    }
}
