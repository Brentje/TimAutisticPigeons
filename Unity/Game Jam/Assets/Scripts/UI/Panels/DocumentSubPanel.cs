﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DocumentSubPanel : UIElement
{
    public UIElement creditsPanel;
    public UIElement highscoresPanel;

    public override void Enter(UIStack stack)
    {
        base.Enter(stack);
    }

    public override void Exit(UIStack stack)
    {
        base.Exit(stack);
    }

    public void OpenCredits()
    {
        if (creditsPanel == null)
            return;

        UIStack.Push(creditsPanel);
    }

    public void OpenHighscores()
    {
        if (highscoresPanel == null)
            return;

        UIStack.Push(highscoresPanel);
    }
}
