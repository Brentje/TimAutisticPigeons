﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements;

public class MouseClick : MonoBehaviour {
	public AudioClip ClickSound;

	public void Update() {
	    if (Input.GetMouseButtonDown((int)MouseButton.LeftMouse))
            AudioManager.Play(ClickSound, AudioType.SFX);
	}
}
