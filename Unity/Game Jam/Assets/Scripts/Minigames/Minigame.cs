﻿using UnityEngine;
using UnityEngine.Events;

public class Minigame : UIElement {
    public AudioClip FailSound;

    protected UnityAction OnSuccess;
    protected UnityAction OnFail;

    public static int Score;

    private void Awake() {
        OnFail += () => {
            AudioManager.Play(FailSound, AudioType.SFX);
        };
    }

    public virtual void Success() {
        OnSuccess?.Invoke();
        UIStack.Push(UIStack.MiniGameFinished);
        Score++;
        AudioManager.StopMusic();
    }

    public virtual void Fail() {
        OnFail?.Invoke();
        UIStack.PopAll();
        UIStack.Push(UIStack.BlueScreen);
        AudioManager.StopMusic();
    }
}
