﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Firewall : Minigame
{
    public AudioClip Music;

    public FireWallPlayer Player;

    public GameObject WallRight;
    public GameObject WallLeft;

    public GameObject Grid;
    public float GridXOffset;
    public float GridYOffset;

    public GameObject PlatformPrefab;
    public float Speed;
    public bool IsPlaying;

    public override void Enter(UIStack stack)
    {
        base.Enter(stack);

        SetupGame();

        AudioManager.Play(Music, AudioType.MUSIC);
        OnSuccess += () => { IsPlaying = false; };
        Play();
    }

    public void Start()
    {
        Play();
    }

    private void SetupGame()
    {
        int lastPos = 6;

        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                int currentPos = Random.Range(1, 5);
                if (currentPos == lastPos)
                    currentPos = Random.Range(0, 6);

                Vector2 tile = new Vector2(Grid.transform.position.x + currentPos * GridXOffset, Grid.transform.position.y + i * GridYOffset);
                GameObject platform = Instantiate(PlatformPrefab, tile, Quaternion.identity);
                platform.transform.SetParent(Grid.transform);

                lastPos = currentPos;
            }
        }
    }

    public void Play()
    {
        IsPlaying = true;

        StartCoroutine(CloseWalls());
    }

    private void Update()
    {
        if (Player.transform.position.x < -530)
        {
            Success();
        }
    }

    private IEnumerator CloseWalls()
    {
        while (IsPlaying)
        {
            WallLeft.transform.position += new Vector3(Time.deltaTime * Speed, 0, 0);
            WallRight.transform.position -= new Vector3(Time.deltaTime * Speed, 0, 0);

            yield return new WaitForFixedUpdate();
        }
    }
}
