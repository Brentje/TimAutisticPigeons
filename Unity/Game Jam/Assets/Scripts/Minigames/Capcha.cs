﻿using UnityEngine;
using UnityEngine.UI;

public class Capcha : Minigame {
    public AudioClip Music;

    public Sprite[] Capcha1;
    public bool[] isCorrect1;
    public string task1;

    public Sprite[] Capcha2;
    public bool[] isCorrect2;
    public string task2;

    public Sprite[] Capcha3;
    public bool[] isCorrect3;
    public string task3;

    public Text InfoText;

    private int chosenCapcha;

    public Image[] images;
    public AudioClip StartErrorClip;

    public void Start()
    {
        AudioManager.Play(StartErrorClip, AudioType.SFX);
        AudioManager.Play(Music, AudioType.MUSIC);
        ShowCapcha();
    }

    private void ShowCapcha()
    {
        Sprite[] sprites = new Sprite[0];
        chosenCapcha = Random.Range(0, 3);
        switch (chosenCapcha)
        {
            case 0:
            sprites = Capcha1;
            InfoText.text = task1;
            break;
            case 1:
            sprites = Capcha2;
            InfoText.text = task2;
            break;
            case 2:
            sprites = Capcha3;
            InfoText.text = task3;
            break;
        }
        if (sprites.Length == 0)
            return;
        for (int i = 0; i < images.Length; i++)
        {
            images[i].sprite = sprites[i];
        }
    }

    public void ButtonPressed(int index)
    {
        bool[] correctBools = new bool[0];
        switch (chosenCapcha)
        {
            case 0:
            correctBools = isCorrect1;
            break;
            case 1:
            correctBools = isCorrect2;
            break;
            case 2:
            correctBools = isCorrect3;
            break;
        }
        if (correctBools.Length != 16)
            return;
        if (correctBools[index])
        {
            images[index].color = Color.green;
            correctBools[index] = false;
            images[index].GetComponent<Button>().enabled = false;
            for (int i = 0; i < correctBools.Length; i++)
            {
                if (correctBools[i])
                    return;
            }
            Success();
        }
        else
        {
            Fail();
        }
    }
}
